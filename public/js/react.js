class Form extends React.Component {
    constructor(props) {
        //Наследуем родителя
        super(props);


        var name = localStorage.getItem("name") || "";
        var nameIsValid = this.validateName(name);
        var phone = localStorage.getItem("phone") || "";
        var area = localStorage.getItem("area") || "";
        var comment = localStorage.getItem("comment") || "";

        this.state = {
            name: name,
            phone: phone,
            area: 1,
            comment: comment,
            nameValid: nameIsValid
        }

        //Биндинг всех методов
        this.changeName = this.changeName.bind(this);
        this.changePhone = this.changePhone.bind(this);
        this.changeComment = this.changeComment.bind(this);
        this.changeArea = this.changeArea.bind(this);
        this.submit = this.submit.bind(this);
        this.resetForm = this.resetForm.bind(this);
        this.blocked = this.blocked.bind(this);
        this.unblocked = this.unblocked.bind(this);
    }

    //Блокировка и разблокировка кнопки "Отправить"
    blocked(btn) {
        btn.disabled = true;
    }
    unblocked(btn) {
        btn.disabled = false;
    }

    validateName(name) {
        return name.length > 0;
    }

    //Изменение имени
    changeName(event) {
        //Берем значение инпута имени
        let val = event.target.value;
        console.log("Имя: " + val);
        //Проверяем валидность имени
        var valid = this.validateName(val);
        //Меняем состояние
        this.setState({
            name: event.target.value,
            nameValid: valid
        });
        //Записываем в LocalStorage
        localStorage.setItem("name", event.target.value);
    }

    //Изменение номера телефона
    changePhone(event) {
        //Берем значение инпута телефона
        var val = event.target.value;
        console.log("Телефон: " + val);
        //Меняем состояние
        this.setState({
            phone: event.target.value
        });
        //Записываем в LocalStorage
        localStorage.setItem("phone", event.target.value);
    }

    //Изменение региона
    changeArea(event) {
        //Берем значение select
        var val = event.target.value;
        console.log("Регион: " + val);
        //Меняем состояние
        this.setState({
            area: event.target.value
        });
        //Записываем в LocalStorage
        localStorage.setItem("area", event.target.value);
    }

    //Изменение комментария
    changeComment(event) {
        // Берем значение поля комментария
        var val = event.target.value;
        // Выводим в консоль введенные символы
        console.log("Comment: " + val);
        // Меняем состояние
        this.setState({
            comment: event.target.value
        });
        //Записываем в LocalStorage
        localStorage.setItem("comment", event.target.value);
    }

    //Очищаем localStorage
    resetForm() {
        // Меняем поля на пустые строки
        localStorage.setItem("name", "");
        localStorage.setItem("phone", "");
        localStorage.setItem("area", "");
        localStorage.setItem("comment", "");
        // Меняем состояние
        this.setState({
            name: "",
            phone: "",
            area: "",
            comment: "",
        });
        //Очищаем инпуты и поля
        $(".name1").value = "";
        //Закрываем форму с помощью кнопки назад
        history.back();
    }

    //Обработчик нажатия кнопки назад
    submit(e) {
        //Запрет обновления страницы
        e.preventDefault();
        //Проверка валидности имени
        if (this.state.nameValid === true) {
            //Включаем блокировку кнопки
            this.blocked(document.getElementById("button-submit"));
            //Отправляем форму на сервер
            fetch("https://formcarry.com/s/VYPDe1IxTiA", {
                method: "POST",
                body: JSON.stringify(this.state),
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
            }).then(
                (response) => (response.json())
            ).then((response) => {
                //Если успешно
                if (response.status === "success") {
                    //Выводим сообщение об отправке
                    alert("Спасибо, сообщение отправлено!");
                    //Убираем блокировку кнопки
                    this.unblocked(document.getElementById("button-submit"));
                    //Очищаем и закрываем форму
                    this.resetForm();
                //Если ошибка
                } else if (response.status === "fail") {
                    //Выводим сообщение об ошибке
                    alert("Ошибка отправки! Пожалуйста, повторите попытку.")
                    //Отключаем бллокировку кнопки
                    this.unblocked(document.getElementById("button-submit"));
                }
            })
        } else {
            alert("Ошибка отправки! Пожалуйста, проверьте правильность введённых данных и повторите попытку снова.");
        }
    }
    render() {
        return (
            <form id="forma" onSubmit={this.submit} method="POST" accept-charset="UTF-8">
                <span id="close">&times;</span>
                <input id="inputName" className="name name1" name="Name" placeholder="ФИО" type="text" value={this.state.name} onChange={this.changeName}/>
                <input id="inputPhone" className="phone name1" name="Number" placeholder="Телефон" type="text" value={this.state.phone} onChange={this.changePhone}/>
                <select id="area" name="Area" value={this.state.value} onChange={this.changeArea}>
                    <option value="1">Регион</option>
                    <option value="2">Московская область</option>
                    <option value="3">Краснодарский край</option>
                    <option value="4">Санкт-Петербург</option>
                    <option value="5">Ставропольский край</option>
                    <option value="6">Ростовская область</option>
                    <option value="7">Хабаровский край</option>
                </select>
                <textarea className="area display-block" name="Message" placeholder="Сообщение" value={this.state.comment} onChange={this.changeComment}/>
                <button id="button-submit" className="contact__buttonF display-block" type="submit">отправить</button>
           </form>
        );
    }
}

ReactDOM.render(<Form />, document.getElementById("form"));