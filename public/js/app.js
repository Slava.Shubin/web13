$(document).ready(function() {
	//Обработка нажатия кнопки
	let blocked = function(btn) {
		btn.disabled = true;
	};
	let unblocked = function(btn) {
		btn.disabled = false;
	};

	formFetch("forma", "button-submit");

	function formFetch(formId, buttonId) {
		//Форма
		let form = document.getElementById(formId);
		//Кнопка
		let button = document.getElementById(buttonId);
		//Ссылка на Formcarry
		let action = form.getAttribute("action");
		//Метод отпрвки формы
		let method = form.getAttribute("method");
		//Данные из полей input
		let inputs = form.querySelectorAll("input");
		//Данные из поля textarea
		let textAreas = form.querySelectorAll("textarea");

		//Обработка нажатия кнопки "Отправить"
		form.onsubmit = async (e) => {
			//Объект данных
			var data = new FormData();
			//Не даём обновить страницу
			e.preventDefault();
			//Блокировка кнопки
			blocked(button);
			//Заполнение объекта data
			for (var i = 0; i < inputs.length; i++) {
				data.append(inputs[i].name, inputs[i].value);
			}
			for (var i = 0; i < textAreas.length; i++) {
				data.append(textAreas[i].name, textAreas[i].value)
			}

			//Отправка с помощью fetch
			let response = await fetch(action, {
				method: method,
				headers: {
					"Accept": "application/json"
				},
				body: data
			});

			//Проверка на успешность отправки
			if (response.ok) {
				let result = await response.json();
				//Очистка
				localStorage.clear();
				//Закрываем модальное окно с формой
				if (!$("#form").hasClass("display-none")) history.back();
				//Разблокировка кнопки
				unblocked(button);
				//Вывод сообщение об успешной отправке
				alert(result.title + "\n" + result.message);
			}
			else {
				//Вывод сообщения об ошибке
				alert("Ошибка отправки!\nПожалуйста, проверьте правильность заполнения формы и повторите отправку.");
				unblocked(button);
			}
		};
	}
})